puts 'Create 10 blogs'
10.times do |blog|
  Blog.create!(
    title: "My blog number: #{blog}",
    body: "Lorem ipsum dolor sit amet."
  )
end
puts '10 blogs created'
puts 'Create 5 skills'
5.times do |skill|
  Skill.create!(
    title: "Skill: #{skill}",
    percent_utilized: skill
  )
end
puts '5 skills created'
puts 'Create 9 portfolios'
1.times do |p|
  Portfolio.create(
    title: "Portfolio Nr.: #{p}",
    subtitle: "Subtitle Nr: #{p}",
    body: "Lorem ipsum dolor sit amet.",
    main_image: "http://via.placeholder.com/600x400",
    thumb_image: "http://via.placeholder.com/350x200"
  )
end
puts '9 portfolios created'
